package homework;
import java.util.HashMap;
import java.util.Scanner;

public class MainClass {
	/*
	 The function "WhoIs" can search a person's name from a list
	 and output the information.
	 */
	public static void WhoIs(String n, Person people[]) {
		int count = 0;
		for (Person person : people) {
			if (n == person.name) {
				person.printinfo();
				break;
			}
			count++;
		}
		if (count == people.length) {
			System.out.println("There is no " + n + " in the system");
		}
	}
	
	/*
	 The function "WhoIs2" can search a person's name from a hashmap
	 and output the information.
	 */
	static HashMap<String, Person> map = new HashMap<>();
    public static void WhoIs2(String n) {
    	if (map.containsKey(n)) {
    		Person someone = map.get(n);
    		someone.printinfo();
    	}
    	else {
    		System.out.println("There is no " + n + " in the system");
    	}
    }
    
    /*
     The function "WhoIs3" provides a search system which can add new person 
     if the system does not have this person.
     */
    public static void WhoIs3() {
    	Scanner sc = new Scanner(System.in);
    	while(true) {
    		System.out.println("Who do you wanna search for? (If you want to "
    				+ "exit, please type exit)");
    		String n = sc.nextLine();
    		if (n.equals("exit")) {
    			System.out.println("Exit Succeed");
    			sc.close();
    			break;
    		}
    		else if (map.containsKey(n)) {
        		Person someone = map.get(n);
        		someone.printinfo();
        		continue;
        	}
    		else {
        		System.out.println("There is no " + n + " in the system. "
        				+ "Do you want to add information for him/her?"
        				+ " (y/n)");
        		 if (sc.nextLine().equals("y")) {
        			 BlueDevil new_p = new BlueDevil();
        			 System.out.println("Name:");
        			 new_p.name = sc.nextLine();
        			 System.out.println("Birthplace:");
        			 new_p.birthplace = sc.nextLine();
        			 System.out.println("Gender (male/female):");
        			 new_p.sex = sc.nextLine();
        			 System.out.println("Hobbies:");
        			 new_p.hobby = sc.nextLine();
        			 System.out.println("Major:");
        			 new_p.major = sc.nextLine();
        			 System.out.println("Undergraduate School:");
        			 new_p.bachelor = sc.nextLine();
        			 System.out.println("NetID:");
        			 new_p.netid = sc.nextLine();
        			 System.out.println("Title:");
        			 new_p.title = sc.nextLine();
        			 System.out.println("Recording has been completed");
        			 map.put(new_p.name, new_p);
        		 }
        		 continue;
    		}
    	}
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person[] list = new Person[8];
        list[0] = new BlueDevil("Tom", "male", "ECE", "AB", "123", 
        		"Phd student", "America", "playing piano");
        list[1] = new BlueDevil("Marry", "female", "BME", "CD", "456", 
        		"undergraduate student", "Africa", "singing");
        list[2] = new BlueDevil("Amy", "female", "ECE", "AB", "789", 
        		"Professor", "China", "watching movies");
        list[3] = new BlueDevil("Kris", "male", "CS", "EF", "111", 
        		"TA", "UK", "sleeping");
        list[4] = new BlueDevil("Adel", "male", "ECE", "NCU", "unknown", 
        		"Adjunct Professor", "unknown", "Tennis, Biking, Gardening "
        		+ "and Cooking");
        list[5] = new BlueDevil("Ric", "male", "CS", "Trinity University, "
        		+ "San Antonio, TX", "unknown", "Adjunct Professor", "USA", 
        		"Golf, Sand Volleyball, Swimming and Biking");
        list[6] = new BlueDevil("Yuanyuan Yu", "female", "ECE", "ECUST", 
        		"unknown", "TA", "China", "baseball and fencing");
        list[7] = new BlueDevil("You Lyu", "male", "ECE", "unknown", "unknown", 
        		"TA", "China", "traveling, music and history");
        
        WhoIs("Tom", list);
        WhoIs("Amy", list);
        WhoIs("who", list);
        
        map.put("Tom", list[0]);
        map.put("Marry", list[1]);
        map.put("Amy", list[2]);
        map.put("Kris", list[3]);
        map.put("Adel", list[4]);
        map.put("Ric", list[5]);
        map.put("Yuanyuan Yu", list[6]);
        map.put("You Lyu", list[7]);
        
        WhoIs2("Kris");
        WhoIs2("who");
        
        WhoIs3();
	}

}
