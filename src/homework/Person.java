package homework;

public abstract class Person {

	protected String name;
	protected String sex;
	protected String birthplace;
	protected String hobby;
	public Person() {
		this.name = "nobody";
		this.sex = "unknown-sex";
		this.birthplace = "unknown-birthplace";
		this.hobby = "unknown-hobbies";
	}
	public Person(String name, String sex, String birthplace, 
			String hobbies) {
		this.name = name;
		this.sex = sex;
		this.birthplace = birthplace;
		this.hobby = hobbies;
	}
	public abstract void printinfo();
}
