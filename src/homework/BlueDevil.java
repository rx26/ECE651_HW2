package homework;

public class BlueDevil extends Person{

	protected String major;
	protected String bachelor;
	protected String netid;
	protected String title;
	public BlueDevil() {
		super();
		this.major = "unknown-major";
		this.bachelor = "unknown-school";
		this.netid = "unknown-netid";
		this.title = "unknown-title";
	}
	public BlueDevil(String name, String sex, String major, 
			String bachelor, String netid, String title,
			String birthplace, String hobbies) {
		super(name, sex, birthplace, hobbies);
		this.major = major;
		this.bachelor = bachelor;
		this.netid = netid;
		this.title = title;
	}
	public String print1() {
		String a = name + " is from " + birthplace + 
				" and is a " + title + ". ";
		return a;
	}
	public String print2() {
		String b = "This guy has no academic information. ";
		if (sex.equals("male")) {
			b = "He was graduated from " + bachelor + 
			" and his Id is " + netid + ". ";
		}
		else if (sex.equals("female")) {
			b = "She was graduated from " + bachelor + 
			" and her Id is " + netid + ". ";
		}
		return b;
	}
	public String print3() {
		String c = "When not in class, " + name + " enjoys " + hobby;
		return c;
	}
	public void printinfo() {
		String info = this.print1() + this.print2() + this.print3();
		System.out.println(info);
	}

}
